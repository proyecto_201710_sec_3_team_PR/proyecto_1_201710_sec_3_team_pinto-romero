package p1_201710.test;

import model.data_structures.Stack;
import junit.framework.TestCase;

public class StackTest extends TestCase
{
	Stack<String> stack = new Stack<>(); 
	
	public void testIsEmpty()
	{
		assertTrue(stack.isEmpty());
	}
	
	public void setupEscenario1()
	{
		stack.push("A");
	}
	
	public void setupEscenario2()
	{
		stack.push("B");
		stack.push("C");
	}
	
	public void testPush()
	{
		//Se prueba con el stack vacio 
		stack.push("A");
		String a = stack.darListaStack().darPrimerNodo().darDatos();
		assertEquals("A", a);
		
		//Se prueba con un elemento en el stack
		stack.push("B");
		String b = stack.darListaStack().darPrimerNodo().darDatos();
		assertEquals("B", b);
	}
	
	public void testPop()
	{
		//Se prueba con un elemento en el stack
		setupEscenario1();
		String a = stack.pop();
		assertEquals("A", a);
				
		//Se prueba con dos elemento en el stack
		setupEscenario2();
		String c = stack.pop();
		assertEquals("C", c);
		String b = stack.pop();
		assertEquals("B", b);
		
		try 
		{
			stack.pop();
			fail();
			
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	public void testSize()
	{
		assertEquals(0, stack.size());
		setupEscenario2();
		assertEquals(2, stack.size());
		stack.pop();
		assertEquals(1, stack.size());
	}
}
