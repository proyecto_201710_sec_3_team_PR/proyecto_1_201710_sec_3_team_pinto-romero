package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListaDobleEncadenada<T> implements ILista<T>{
	
	private NodoDoble<T> primerNodo;
	private int size;
	
	public ListaDobleEncadenada()
	{
		primerNodo = null;
		size = 0;
	}
	
	public boolean estaVacia()
	{
		return (size == 0) ? true : false;
	}
	
	private class NodoDoble<U>
	{
		private NodoDoble<U> siguienteNodo;
		private NodoDoble<U> nodoAnterior;
		private int index;
		private U datos;
		
		public U darDatos()
		{
			return this.datos;
		}
		
		private NodoDoble(U datosN)
		{
			this.datos = datosN;
			this.siguienteNodo = null;
			this.nodoAnterior = null;
		}
		
		public NodoDoble<U> darNodoSiguiente()
		{
			return this.siguienteNodo;
		}
		
		public NodoDoble<U> darNodoAnterior()
		{
			return this.nodoAnterior;
		}
		
		public void cambiarSiguiente(NodoDoble<U> nodo)
		{
			this.siguienteNodo = nodo;
		}
		
		public void cambiarAnterior(NodoDoble<U> nodo)
		{
			this.nodoAnterior = nodo;
		}
		
		public boolean agregarNodo(NodoDoble<U> nodo)
		{
			NodoDoble<U> nuevo = nodo;
			nodo.index = size;
			nuevo.cambiarAnterior(this);
			nuevo.cambiarSiguiente(this.siguienteNodo);
			this.cambiarSiguiente(nodo);
			return true;
		}
		
		public boolean eliminarNodo()
		{
			if(this.siguienteNodo != null)
			{
				this.siguienteNodo.cambiarAnterior(this.darNodoAnterior());
			}
			
			if (this.nodoAnterior != null) 
			{
				nodoAnterior.cambiarSiguiente(this.darNodoSiguiente());
			}
			
			this.cambiarAnterior(null);
			this.cambiarSiguiente(null);
			
			return true;
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>()
				{
			NodoDoble<T> actual = null;
			@Override
			public boolean hasNext() 
			{	
				boolean has = false;
				if(estaVacia())
				{
					return has;
				}
				else if(actual == null)
				{
					return true;
				}
				else if(actual.darNodoSiguiente() != null)
				{
					has = true;
				}
				return has;	
			}
			@Override
			public T next() 
			{	
				if (estaVacia()) 
				{
					throw new NoSuchElementException();
				}
				else if(actual == null)
				{
					actual = primerNodo;
					return actual.datos;
				}
				else if(actual.darNodoSiguiente() == null)
				{
					throw new NoSuchElementException();
				}
				
				this.actual = actual.darNodoSiguiente();
				return actual.datos;
			}	
				};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> nodo = new NodoDoble<T>(elem);
		
		if(estaVacia())
		{
			nodo.index = 0;
			this.primerNodo = nodo;
			size ++;
		}
		else
		{
			NodoDoble<T> nodoTemporal = primerNodo;
			
			while(nodoTemporal.darNodoSiguiente()!=null)
			{
				nodoTemporal = nodoTemporal.darNodoSiguiente();
			}
			if(nodoTemporal.agregarNodo(nodo))
			{
				size++;	
			}	
		}
	}

	@Override
	public T darElemento(int pos) {
		T devuelve = null;
		if(!estaVacia())
		{
			NodoDoble<T> nodoTemporal = primerNodo;
			
			if(primerNodo.index == pos)
			{
				devuelve = primerNodo.datos;
			}
			else
			{
				boolean encontro = false;
				
				while(nodoTemporal.darNodoSiguiente() != null && !encontro)
				{
					nodoTemporal = nodoTemporal.darNodoSiguiente();
					if(nodoTemporal.index == pos)
					{
						devuelve = nodoTemporal.darDatos();
						encontro = true;
					}
				}	
			}
		}
		return devuelve;
	}

	@Override
	public T eliminarElemento(int pos) {
		T devuelve = null;
		NodoDoble<T> nodo = null;
		
		System.out.println("entra");
		if(estaVacia())
		{
			throw new NoSuchElementException();
		}
		
		if(primerNodo.index == pos)
		{
			nodo = primerNodo;
		}
		else
		{
			NodoDoble<T> nodoTemporal = primerNodo;
			boolean encontro = false;
			
			while(!encontro && nodoTemporal.darNodoSiguiente() != null)
			{
				nodoTemporal = nodoTemporal.darNodoSiguiente();
				if(nodoTemporal.index == pos)
				{
					nodo = nodoTemporal;
					encontro = true;
				}
			}	
		}
		
		if(nodo != null && nodo.eliminarNodo())
		{
			nodo.eliminarNodo();
			devuelve = nodo.darDatos();
			size--;
		}
		return devuelve;
	}

	@Override
	public int darNumeroElementos() {
		return size;
	}
}
