package model.vo;

import java.sql.Timestamp;

public class VORating implements Comparable<VORating> {
	
	private long idUsuario;
	private long idPelicula;
	private Timestamp timeStamp;
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(Timestamp timeStamp) {
		
		this.timeStamp = timeStamp;
	}
	private double rating;
	
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public double getRating() {
		return rating;
	}
	public void setRating(double rating) {
		this.rating = rating;
	}
	@Override
	public int compareTo(VORating voR) 
	{
		//De m�s reciente a menos reciente
		if(timeStamp.compareTo(voR.getTimeStamp()) > 0)
		{
			return -1;
		}
		else 
		{
			return 1;
		}
	}
}
