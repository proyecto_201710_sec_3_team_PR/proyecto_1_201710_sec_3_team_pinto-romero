package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.sun.org.apache.xpath.internal.WhitespaceStrippingElementMatcher;

public class ListaEncadenada<T> implements ILista<T>{
	
	private NodoSencillo<T> primerNodo;
	private int size;
	
	public ListaEncadenada(){
		primerNodo = null;
		size = 0;
	}
	
	public boolean estaVacia(){
		return (size == 0) ? true : false;
	}
	
	public NodoSencillo<T> darPrimerNodo() {
		return primerNodo;
	}
	
	public NodoSencillo<T> darNodo(int pos)
	{
		NodoSencillo<T> devuelve = null;
		if(!estaVacia())
		{
			NodoSencillo<T> nodoTemporal = primerNodo;
			if(primerNodo.index == pos)
			{
				devuelve = primerNodo;
			}
			else
			{
				boolean encontro = false;
				while(nodoTemporal.darNodoSiguiente() != null && !encontro)
				{
					nodoTemporal = nodoTemporal.darNodoSiguiente();
					if(nodoTemporal.index == pos)
					{
						devuelve = nodoTemporal;
						encontro = true;
					}
				} 
			}
		}
	return devuelve;
	}
	
	public class NodoSencillo<U> {
		private NodoSencillo<U> siguienteNodo;
		private int index;
		private U datos;
		
		private NodoSencillo(U datosN)
		{
			this.datos = datosN;
			this.siguienteNodo = null;
		}
		
		public U darDatos()
		{
			return this.datos;
		}
		
		public void modificarDatos(U datosN)
		{
			this.datos = datosN;
		}
		
		public NodoSencillo<U> darNodoSiguiente()
		{
			return this.siguienteNodo;
		}
		
		public int darIndex()
		{
			return this.index;
		}
		
		public void cambiarSiguiente(NodoSencillo<U> nodo)
		{
			this.siguienteNodo = nodo;
		}
		
		public boolean agregarNodo(NodoSencillo<U> nodo)
		{
			this.cambiarSiguiente(nodo);
			return true;
		}
		
		public boolean eliminarNodoSiguiente()
		{
				this.cambiarSiguiente(siguienteNodo.darNodoSiguiente());
				
				return true;
		}
		
		public void modificarIndex(int in)
		{
			this.index = in;
		}
	}

	@Override
	public Iterator<T> iterator() 
	{
		return new Iterator<T>()
				{
					NodoSencillo<T> actual = null;
					@Override
					public boolean hasNext() 
					{	
						boolean has = false;
						if(estaVacia())
						{
							return has;
						}
						else if(actual == null)
						{
							return true;
						}
						else if(actual.darNodoSiguiente() != null)
						{
							has = true;
						}
						return has;	
					}
					@Override
					public T next() 
					{
						if (estaVacia()) 
						{
							throw new NoSuchElementException();
						}
						else if(actual == null)
						{
							actual = primerNodo;
							return actual.datos;
						}
						else if(actual.darNodoSiguiente() == null)
						{
							throw new NoSuchElementException();
						}
						
						this.actual = actual.darNodoSiguiente();
						return actual.datos;
					}		
				};
	}

	@Override
	public void agregarElementoFinal(T elem) {
		if(elem != null)
		{
			NodoSencillo<T> nodo = new NodoSencillo<T>(elem);
			
			if(estaVacia())
			{
				this.primerNodo = nodo;
				nodo.index = 0;
				size ++;
			}
			else
			{
				NodoSencillo<T> nodoTemporal = primerNodo;

				while(nodoTemporal.darNodoSiguiente() != null)
				{
					nodoTemporal = nodoTemporal.darNodoSiguiente();
				}
				
				nodo.index = size;
				if(nodoTemporal.agregarNodo(nodo))
				{
					size++;
				}	
			}
		}
		
	}

	@Override
	public T darElemento(int pos) {
		T devuelve = null;
		if(!estaVacia())
		{
			NodoSencillo<T> nodoTemporal = primerNodo;
			
			if(primerNodo.index == pos)
			{
				devuelve = primerNodo.datos;
			}
			else
			{
				boolean encontro = false;
				
				while(nodoTemporal.darNodoSiguiente() != null && !encontro)
				{
					nodoTemporal = nodoTemporal.darNodoSiguiente();
					if(nodoTemporal.index == pos)
					{
						devuelve = nodoTemporal.datos;
						encontro = true;
					}
				}	
			}
		}
		return devuelve;
	}

	@Override
	public T eliminarElemento(int pos) {
		T devuelve = null;
		if(!estaVacia())
		{
			NodoSencillo<T> nodoTemporal = primerNodo;
			NodoSencillo<T> nodoAnterior = null;
			
			if(primerNodo.index == pos)
			{
				devuelve = primerNodo.datos;
				primerNodo = primerNodo.darNodoSiguiente();
				size--;
				modificarIndex();
			}
			else
			{
				boolean encontro = false;
				
				while( !encontro && nodoTemporal.darNodoSiguiente() != null )
				{
					nodoAnterior = nodoTemporal;
					nodoTemporal = nodoTemporal.darNodoSiguiente();
					if(nodoTemporal.index == pos && nodoAnterior.eliminarNodoSiguiente())
					{
						devuelve = nodoTemporal.datos;
						nodoTemporal = nodoAnterior.darNodoSiguiente();
						size--;
						encontro = true;
						modificarIndex();
					}
				}
			}
		}
		return devuelve;
	}
	
	public void agregarElementoAlInicio(T elem)
	{
		NodoSencillo<T> nodo = new NodoSencillo<T>(elem);
		
		if(estaVacia())
		{
			nodo.index = 0;
			this.primerNodo = nodo;
			size++;
		}
		else
		{
			NodoSencillo<T> nodoT = primerNodo;
			nodo.agregarNodo(nodoT);
			this.primerNodo = nodo;
			modificarIndex();
			size++;
		}
	}
	
	public void modificarIndex()
	{
		if(primerNodo != null)
		{
			primerNodo.modificarIndex(0);
			NodoSencillo nodo = primerNodo;
			int a = 1;
			while(nodo.darNodoSiguiente() != null)
			{
				nodo = nodo.darNodoSiguiente();
				nodo.modificarIndex(a);
				a++;
			}
		}
		
	}
	@Override
	public int darNumeroElementos() {
		return size;
	}

}
