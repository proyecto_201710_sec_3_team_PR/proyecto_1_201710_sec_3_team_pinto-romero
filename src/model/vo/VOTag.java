package model.vo;

import java.sql.Timestamp;

public class VOTag implements Comparable<VOTag>{
	private String tag;
	private Timestamp timestamp;
	private long movieId;
	private long userId;
	
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public void setMovieID(long idpelicula)
	{
		this.movieId = idpelicula;
	}
	public long getMovieID()
	{
		return movieId;
	}
	public long getUserId()
	{
		return userId;
	}
	public void setUserId(long user)
	{
		this.userId = user;
	}
	@Override
	public int compareTo(VOTag vo) 
	{
		// TODO Auto-generated method stub
		if(timestamp.compareTo(vo.getTimestamp()) > 0)
		{
			return -1;
		}
		else 
		{
			return 1;
		}
	}
	
}
