package model.data_structures;

import model.data_structures.ListaEncadenada.NodoSencillo;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VORating;

public class QuickSortList<T>
{
	public static final String QTRATING = "Fecha de reaci�n de rating";
	public static final String PQRATING = "Promedio Rating";
	public static final String NOMBRE = "Nombre";
	public static final String FECHA = "A�o de publicaci�n";
	public static final String NRATING = "N�mero de ratings";
	public static final String APT = "Ordenar por a�o, promedio y titulo";
	public static final String DIFERAT = "Diferencia de rating"; 
	private static String ordenar;
	
	public static <T extends Comparable<T>> void sort(ListaEncadenada<T> lista, String expresion)
	{
		ordenar = expresion;
		
		quicksort(lista,0, lista.darNumeroElementos()-1);
	}
	
	private static <T extends Comparable<T>> void quicksort(ListaEncadenada<T> lista, int low, int high)
	{
		if(high<=low) return;
		int pivote = partition(lista, low, high);
		quicksort(lista, low, (pivote-1));
		quicksort(lista, pivote+1, high);
	}
	
	private static <T extends Comparable<T>> int partition(ListaEncadenada<T> lista, int low, int high)
	{
		int i = low+1;
		int j = high;
		while(i <= j)
		{
			if( compare(lista.darElemento(i), lista.darElemento(low)) <= 0)
			{
				i++;
			}
			else if( compare(lista.darElemento(j), lista.darElemento(low)) > 0)
			{
				j--;
			}
			else if( j < 1)
			{
				break;
			}
			else
			{
				exchange(lista,i,j);
			}
		}
		exchange(lista, low, j);
		return j;
	}
	
	private static <T extends Comparable<T>> int compare(T primero, T segundo)
	{
		if(ordenar.equals(NOMBRE) || ordenar.equals(QTRATING))
		{
			return primero.compareTo(segundo);
		}
		else if(ordenar.equals(PQRATING) || ordenar.equals(FECHA) || ordenar.equals(NRATING) || ordenar.equals(DIFERAT))
		{
			VOPelicula primer = (VOPelicula) primero;
			VOPelicula segun = (VOPelicula)segundo;
			
			Double d1 = 0.0;
			Double d2 = 0.0;
			
			if(ordenar.equals(PQRATING))
			{
				//De mayor a menor
				d1 = primer.getPromedioRatings();
				d2 = segun.getPromedioRatings();
			}
			else if(ordenar.equals(FECHA))
			{
				//De menor a mayor
				d2 = (double) primer.getAgnoPublicacion();
				d1 = (double) segun.getAgnoPublicacion();
			}
			else if(ordenar.equals(NRATING))
			{
				//De mayor a menor
				d1 = (double) primer.getNumeroRatings();
				d2 = (double) segun.getNumeroRatings();
			}
			else if(ordenar.equals(DIFERAT))
			{
				//De menor a mayor
				d2 = primer.getDiferencia();
				d1 = segun.getDiferencia();
			}
			
			if (d1 > d2) 
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
		else 
		{
			return 0;
		}
		
	}
	
	private static <T extends Comparable<T>> void exchange(ListaEncadenada<T> lista, int i, int j)
	{
		NodoSencillo temp1 = lista.darNodo(i);
		NodoSencillo temp2 = lista.darNodo(j);
		
		Object item1 = temp1.darDatos();
		Object item2 = temp2.darDatos();
		
		temp1.modificarDatos(item2);
		temp2.modificarDatos(item1);
	}
	
	public static boolean estaOrdenada(ListaEncadenada<VOPelicula> lista, String valor)
	{
		boolean si = true;
		Double a = 0.0;
		Double b = 0.0;
		if(!lista.estaVacia())
		{
			VOPelicula vop1 = lista.darElemento(0);
			for (int i = 1; i < lista.darNumeroElementos(); i++) 
			{
				VOPelicula vop2 = lista.darElemento(i);
				if(valor.equals(DIFERAT))
				{
					a = vop1.getDiferencia();
					b = vop2.getDiferencia();
				}
				
				if(b < a)
				{
					return false;
				}
					
			}
		}
			
		return si;
	}
}
