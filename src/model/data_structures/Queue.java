package model.data_structures;

public class Queue<T> 
{
	private ListaEncadenada<T> listaQueue;
	
	public ListaEncadenada<T> darListaQueue()
	{
		return listaQueue;
	}
	
	public Queue()
	{
		listaQueue = new ListaEncadenada<T>();
	}
	
	public void enqueu(T item)
	{
		listaQueue.agregarElementoFinal(item);
	}
	
	public T dequeue()
	{
		return listaQueue.eliminarElemento(listaQueue.darPrimerNodo().darIndex());
	}
	
	public boolean isEmpty()
	{
		return listaQueue.estaVacia();
	}
	
	public int size()
	{
		return listaQueue.darNumeroElementos();
	}
}
