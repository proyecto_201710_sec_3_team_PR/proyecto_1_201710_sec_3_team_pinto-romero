package p1_201710.test;

import model.data_structures.Queue;
import junit.framework.TestCase;

public class QueueTest extends TestCase
{
	Queue<String> queue = new Queue<>();
	
	public void testIsEmpty()
	{
		assertTrue(queue.isEmpty());
	}	
	
	public void setupEscenario1()
	{
		queue.enqueu("A");
	}
	
	public void setupEscenario2()
	{
		queue.enqueu("B");
		queue.enqueu("C");
	}
	
	public void testEnqueu()
	{
		//Se prueba con el queue vacio 
		queue.enqueu("A");
		String a = queue.darListaQueue().darElemento(queue.size());
		assertEquals("A", a);
		
		//Se prueba con un elemento en el queue
		queue.enqueu("B");
		String b = queue.darListaQueue().darElemento(queue.size());
		assertEquals("B", b);
	}
	
	public void testDequeue()
	{
		//Se prueba con un elemento en el queue
		setupEscenario1();
		String a = queue.dequeue();
		assertEquals("A", a);
				
		//Se prueba con dos elemento en el queue
		setupEscenario2();
		String b = queue.dequeue();
		assertEquals("B", b);
		String c = queue.dequeue();
		assertEquals("C", c);
		
		try 
		{
			queue.dequeue();
			fail();
			
		} catch (Exception e) 
		{
			// TODO: handle exception
		}
	}
	
	public void testSize()
	{
		assertEquals(0, queue.size());
		setupEscenario2();
		assertEquals(2, queue.size());
		queue.dequeue();
		assertEquals(1, queue.size());
	}
}
