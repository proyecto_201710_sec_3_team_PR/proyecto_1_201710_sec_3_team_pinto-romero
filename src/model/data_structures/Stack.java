package model.data_structures;
public class Stack<T>
{
	public ListaEncadenada<T> listaStack;
	
	public ListaEncadenada<T> darListaStack()
	{
		return listaStack;
	}
	
	public Stack()
	{
		listaStack = new ListaEncadenada<T>();	
	}

	public void push(T item)
	{
		listaStack.agregarElementoAlInicio(item);
	}
	
	public T pop()
	{
		return listaStack.eliminarElemento(listaStack.darPrimerNodo().darIndex());
	}
	
	public boolean isEmpty()
	{
		return listaStack.estaVacia();
	}
	
	public int size()
	{
		return listaStack.darNumeroElementos();
	}
	public void prueba()
	{
		
	}
}
