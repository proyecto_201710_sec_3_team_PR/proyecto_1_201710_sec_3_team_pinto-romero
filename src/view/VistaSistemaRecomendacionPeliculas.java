package view;

import java.util.Scanner;
import model.data_structures.ILista;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VORating;
import controller.Controller;

public class VistaSistemaRecomendacionPeliculas {
	
	//Se toma "Inicializar peliculas" como una unica operaci�n agrupadora
	static final String OP_0 = "inicializarPeliculas";
	static final String OP_1 = "peliculasPopulares";
	static final String OP_2 = "catalogoPeliculasOrdenadoSR";
	static final String OP_3 = "recomendarGeneroSR";
	static final String OP_4 = "opinionRatingsGeneroSR";
	static final String OP_5 = "recomendarPeliculasSR";
	static final String OP_6 = "ratingsPeliculaSR";
	static final String OP_7 = "usuariosActivosSR";
	static final String OP_8 = "catalogoUsuariosOrdenadoSR";
	static final String OP_9 = "recomendarTagsGeneroSR";
	static final String OP_10 = "opinionTagsGeneroSR";
	static final String OP_11 = "recomendarUsuariosSR";
	static final String OP_12 = "tagsPeliculaSR";
	
	static Long ts1, ts2, val;
	static int valor;
	static String expresion;
	
	public static void main(String[] args) {
		
		Controller.limpiarHistorialOperaciones();
		System.out.println("Gabriel Pinto y Juan Romero");
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("Proyecto 1 - Sistema de Recomendaci�n de Peliculas ");
		System.out.println("Presione la tecla \"0\" seguido de Enter, para inicializar el Sistema de Recomendaci�n de Peliculas (Cargar peliculas, ratings y tags) ");

		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		boolean imprimir = false;
		ILista<VOGeneroPelicula> lisGeneros;
		ILista<VOPelicula> lisPelis;
		ILista<VOPeliculaPelicula> lisPelisPelis;
		ILista<VORating> lisRating;
		while(!fin){
			
			if(imprimir)
			{
				System.out.println("");
				printMenu();
			}
			
			int option = sc.nextInt();
			
			switch(option){
				case 0:
					ts1 = medirTiempo();System.out.println("Por favor espere mientras se carga el sistema");Controller.inicializarSistemaRecomendacion();ts2 = medirTiempo();agregarOperacion(OP_0);imprimir=true;
					break;
				case 1:
					System.out.println("Ingrese el n�mero de peliculas populares que desea ver por g�nero:");expresion = sc.next();valor = Integer.parseInt(expresion);
					espere();ts1 = medirTiempo(); lisGeneros = Controller.peliculasPopulares(valor);ts2 = medirTiempo();Controller.imprimirGenerosPeliculas(lisGeneros, Controller.NRATI);agregarOperacion(OP_1);
					break;
				case 2:
					System.out.println("Se mostrar� todo el catalogo de peliculas...");espere();ts1 = medirTiempo();lisPelis = Controller.catalogoPeliculasOrdenadoSR();ts2 = medirTiempo();Controller.imprimirPeliculas(lisPelis, Controller.APT);agregarOperacion(OP_2);
					break;
				case 3:
					espere();ts1 = medirTiempo();lisGeneros = Controller.recomendarGeneroSR();ts2 = medirTiempo();Controller.imprimirGenerosPeliculas(lisGeneros, Controller.PRATI);agregarOperacion(OP_3);
					break;
				case 4:
					espere();ts1 = medirTiempo();lisGeneros = Controller.opinionRatingsGeneroSR();ts2 = medirTiempo();Controller.imprimirGenerosPeliculas(lisGeneros, Controller.TAGS);agregarOperacion(OP_4);
					break;
				case 5:
					System.out.println("Ingrese el n�mero de peliculas de las cuales desea obtener una recomendaci�n: ");expresion = sc.next();valor = Integer.parseInt(expresion);
					espere();ts1 = medirTiempo();lisPelisPelis = Controller.recomendarPeliculasSR(valor);ts2 = medirTiempo();Controller.imprimirPeliculasPeliculas(lisPelisPelis);agregarOperacion(OP_5);
					break;
				case 6:
					System.out.println("Ingrese el id de la pelicula d e la cual desea observar sus calificaciones: ");expresion = sc.next();val = Long.parseLong(expresion);
					ts1 = medirTiempo();lisRating = Controller.ratingsPeliculaSR(val);Controller.imprimirRatings(lisRating, val);ts2 = medirTiempo();agregarOperacion(OP_6);
					break;
				case 7:
					ts1 = medirTiempo();ts2 = medirTiempo();Controller.catalogoUsuariosOrdenadoSR();agregarOperacion(OP_7);
					break;
				case 8:
					ts1 = medirTiempo();ts2 = medirTiempo();agregarOperacion(OP_8);
					break;
				case 9:
					ts1 = medirTiempo();ts2 = medirTiempo();agregarOperacion(OP_9);
					break;
				case 10:
					ts1 = medirTiempo();ts2 = medirTiempo();agregarOperacion(OP_10);
					break;
				case 11:
					ts1 = medirTiempo();ts2 = medirTiempo();agregarOperacion(OP_11);
					break;
				case 12:
					ts1 = medirTiempo();ts2 = medirTiempo();agregarOperacion(OP_12);
					break;
				case 13:
					System.out.println("N�mero de operaciones realizadas: "+ Controller.cantidadOperaciones());System.out.println("Operaciones relizadas:");
					Controller.imprimirDatosOperaciones(Controller.darHistoralOperacionesSR());
					break;
				case 14:
					Controller.limpiarHistorialOperaciones();
					break;
				case 15:
					System.out.println("Operaciones realizadas: "+Controller.cantidadOperaciones());System.out.println("Ingrese la cantidad de operaciones que desea ver:");expresion = sc.next();valor = Integer.parseInt(expresion);
					Controller.imprimirDatosOperaciones(Controller.darUltimasOperaciones(valor));
					break;
				case 16:
					System.out.println("Operaciones realizadas: "+Controller.cantidadOperaciones());System.out.println("Ingrese la cantidad de operaciones que desea borrar:");expresion = sc.next();valor = Integer.parseInt(expresion);
					Controller.borrarUltimasOperaciones(valor);System.out.println(valor+" operaciones, han sido borradas");
					break;
				case 17:
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		
		System.out.println("Escriba el n�mero de la opci�n que quiere realizar");
		System.out.println("1. Mostrar las \"n\" peliculas m�s populares por gen�ro");
		System.out.println("2. Generar un catalogo de peliculas ordenado por a�o, promedio y nombre");
		System.out.println("3. Mostrar las peliculas con la mejor calificaci�n promedio por genero");
		System.out.println("4. Generar un catalogo de peliculas ordenado por fecha, con los tags asociados a ellas");
		System.out.println("5. Peliculas sugeridas ordenadas por rating");
		System.out.println("6. Calificaciones de un pelicula ordenadas por fecha de creaci�n");
		System.out.println("7. Mostrar los N usuarios con mas ratings creados por genero");
		System.out.println("8. Generar una lista de usuarios ordenada segun parametros");
		System.out.println("9. Crear una lista de peliculas con el mayor numero de tags por cada genero");
		System.out.println("10. Generar una lista con los usuarios y los tags que ha creado");
		System.out.println("11. Dar una lista de usuarios que le pueden interesar");
		System.out.println("12. tags de una pelicula especifica");
		System.out.println("13. Dar historial de operaciones");
		System.out.println("14. Limpiar historial de operaciones");
		System.out.println("15. Dar las ultimas \"n\" operaciones");
		System.out.println("16. Borrar las ultimas \"n\" operaciones");
		System.out.println("17. Salir ");
	}
	
	private static Long medirTiempo()
	{
		return System.currentTimeMillis();
	}
	
	private static void espere()
	{
		System.out.println("Por favor espere...");
	}
	
	private static void agregarOperacion(String OP)
	{
		Controller.agregarOperacionSR(OP,ts1,ts2);
	}
}
