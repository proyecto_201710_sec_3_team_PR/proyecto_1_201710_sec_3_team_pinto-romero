package p1_201710.test;

import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada;
import junit.framework.TestCase;

public class ListaDobleEncadenadaTest extends TestCase
{

	private ListaDobleEncadenada<String> lista;
	
	public void setupListaVacia()
	{
		lista = new ListaDobleEncadenada<>();
	}
	
	public void setupLista1()
	{
		setupListaVacia();
		lista.agregarElementoFinal("a");
	}
	
	public void setupLista2()
	{
		setupLista1();
		lista.agregarElementoFinal("b");
		lista.agregarElementoFinal("c");
	}
	
	public void testEstaVacia()
	{
		setupListaVacia();
		assertTrue(lista.estaVacia());
	}
	
	public void testAgregarElementoAlFinal()
	{
		setupListaVacia();
		lista.agregarElementoFinal("a");
		lista.agregarElementoFinal("b");
		
		Iterator<String> iter = lista.iterator();
		
		String ultimo = "";
		
		while(iter.hasNext())
		{
			ultimo = iter.next();
		}
		
		assertEquals("b", ultimo );
		
	}
	
	public void testDarElemento()
	{
		setupLista2();
		assertEquals("a", lista.darElemento(1));
		assertEquals("b", lista.darElemento(2));
		assertEquals("c", lista.darElemento(3));
	}
	
	public void testDarNumeroElmentos()
	 {
		 setupListaVacia();
		 assertEquals(0, lista.darNumeroElementos());
		 setupLista1();
		 assertEquals(1, lista.darNumeroElementos());
		 setupLista2();
		 assertEquals(3, lista.darNumeroElementos());
	 }
	
	public void testEliminarElmento()
	{
		setupLista1();
		assertEquals("a", lista.eliminarElemento(1));
		
		setupLista2();
		assertEquals("c", lista.eliminarElemento(3));
	}
	
}