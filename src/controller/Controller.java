package controller;

import model.data_structures.ILista;
import model.logic.SistemaRecomendacionPeliculas;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;

import java.sql.Time;
import java.sql.Timestamp;

import api.ISistemaRecomendacionPeliculas;

public class Controller 
{	
	
	public static final String NRATI = "Cantidad de ratings: ";
	public static final String PRATI = "Rating promedio: ";
	public static final String TAGS = "Tags";
	public static final String RECOMENDADA = "Peliculas recomendadas";
	public static final String APT = "Ordenada por a�o, promedio y titulo";
	private static ISistemaRecomendacionPeliculas manejador= new SistemaRecomendacionPeliculas();

	public static void inicializarSistemaRecomendacion()
	{
		manejador.cargarPeliculasSR("data/movies.csv");	
		System.out.println("Peliculas cargadas... 33%");
		manejador.cargarRatingsSR("data/ratings.csv");
		System.out.println("Ratings de peliculas cargados... 66%");
		manejador.cargarTagsSR("data/tags.csv");
		System.out.println("Tags cargados... 100%");	
	}
	
	public static ILista<VOGeneroPelicula> peliculasPopulares(int n)
	{
		return manejador.peliculasPopularesSR(n);
	}
	
	public static ILista<VOPelicula> catalogoPeliculasOrdenadoSR()
	{
		return manejador.catalogoPeliculasOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarGeneroSR()
	{
		return manejador.recomendarGeneroSR();
	}
	
	public static ILista<VOGeneroPelicula> opinionRatingsGeneroSR()
	{
		return manejador.opinionRatingsGeneroSR();
	}
	
	public static ILista<VOPeliculaPelicula> recomendarPeliculasSR(int n)
	{
		return manejador.recomendarPeliculasSR("data/recomendacion.csv", n);
	}
	
	public static ILista<VORating> ratingsPeliculaSR(long idPelicula)
	{
		return manejador.ratingsPeliculaSR(idPelicula);
	}
	
	public static ILista<VOGeneroUsuario> usuariosActivosSR(int n)
	{
		return manejador.usuariosActivosSR(n);
	}
	
	public static ILista<VOUsuario> catalogoUsuariosOrdenadoSR()
	{
		return manejador.catalogoUsuariosOrdenadoSR();
	}
	
	public static ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n)
	{
		return manejador.recomendarTagsGeneroSR(n);
	}
	
	public static ILista<VOUsuarioGenero> opinionTagsGeneroSR()
	{
		return manejador.opinionTagsGeneroSR();
	}
	
	public static ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) 
	{
		return manejador.recomendarUsuariosSR(rutaRecomendacion, n);
	}
	
	public static ILista<VOTag> tagsPeliculaSR(int idPelicula)
	{
		return manejador.tagsPeliculaSR(idPelicula);
	}
	
	public static void agregarOperacionSR(String nomOperacion, long tinicio, long tfin)
	{
		manejador.agregarOperacionSR(nomOperacion, tinicio, tfin);
	}
	
	public static ILista<VOOperacion> darHistoralOperacionesSR()
	{
		return manejador.darHistoralOperacionesSR();
	}
	
	public static void limpiarHistorialOperaciones()
	{
		manejador.limpiarHistorialOperacionesSR();
	}

	public static ILista<VOOperacion> darUltimasOperaciones(int n)
	{
		return manejador.darUltimasOperaciones(n);
	}
	
	public static void borrarUltimasOperaciones(int n)
	{
		manejador.borrarUltimasOperaciones(n);
	}
	
	public static int cantidadOperaciones()
	{
		return darHistoralOperacionesSR().darNumeroElementos();
	}
	
	//Impresiones
	public static void imprimirDatosOperaciones(ILista<VOOperacion> listaOper)
	{
		for (int i = 0; i < listaOper.darNumeroElementos(); i++) {
			VOOperacion oper = listaOper.darElemento(i);
			Long tiempo = oper.getTimestampFin()-oper.getTimestampInicio();
			System.out.println(oper.getOperacion() + " - Tiempo transcurrido: " + tiempo + " milisegundos");
		}
	}
	
	public static void imprimirPeliculas(ILista<VOPelicula> listaPelis, String tipo)
	{
		for (int j = 0; j < listaPelis.darNumeroElementos(); j++) {
			VOPelicula voP = listaPelis.darElemento(j);
			if( tipo.equals(APT))
			{
				System.out.println(voP.getAgnoPublicacion() + " - " + voP.getPromedioRatings() + " - " + voP.getTitulo());
			}
			
		}
		System.out.println(listaPelis.darNumeroElementos());
	}
	
	public static void imprimirPeliculasPeliculas(ILista<VOPeliculaPelicula> listPelisP)
	{
		for (int i = 0; i < listPelisP.darNumeroElementos(); i++) {
			VOPeliculaPelicula voPP = listPelisP.darElemento(i);
			VOPelicula vOpeli = voPP.getPelicula();
			System.out.println("Pelicula analizada: " + vOpeli.getTitulo() + " - G�nero: "+ vOpeli.getGenerosAsociados().darElemento(0) +" - "+PRATI+ vOpeli.getRatingAsinadoRecomendacion());
			int cantidad = 0;
			ILista<VOPelicula> vOps = voPP.getPeliculasRelacionadas();
			if(vOps != null)
			{
				cantidad = vOps.darNumeroElementos();
			}
			System.out.println(RECOMENDADA+"("+cantidad+")"+"\n");
			if(cantidad != 0)
			{
				int mostrar = 4;
				if(cantidad < 4)
				{
					mostrar = cantidad;
				}
				System.out.println("Se muestras las primeras "+ mostrar +" peliculas");
				for (int j = 0; j < mostrar; j++) {
					VOPelicula voP = vOps.darElemento(j);
					System.out.println(voP.getTitulo() + " - " + voP.getGenerosAsociados().darElemento(0) + " - " + voP.getPromedioRatings());
				}
				System.out.println("\n");
			}
		}
	}
	
	public static void imprimirGenerosPeliculas(ILista<VOGeneroPelicula> listaGeneros, String tipo)
	{
		for (int i = 0; i < listaGeneros.darNumeroElementos(); i++) {
			VOGeneroPelicula voG = listaGeneros.darElemento(i);
			System.out.println("\n"+voG.getGenero()+": \n ");
			ILista<VOPelicula> pelisGene = voG.getPeliculas();
			for (int j = 0; j < pelisGene.darNumeroElementos(); j++) {
				VOPelicula voP = pelisGene.darElemento(j);
				if(tipo.equals(PRATI))System.out.println(voP.getTitulo()+ " - "+ PRATI + voP.getPromedioRatings()); 
				else if(tipo.equals(NRATI))System.out.println(voP.getTitulo()+ " - "+ NRATI + voP.getNumeroRatings() + "\n");
				else if(tipo.equals(TAGS))
				{
					int nTag = voP.getNumeroTags();
					System.out.println(voP.getAgnoPublicacion() + " - " + voP.getTitulo() + " - " + TAGS + " ("+ nTag + ")");
					String tag = "";
					if(nTag > 0)
					{
						ILista<String> tags = voP.getTagsAsociados();
						for (int k = 0; k < tags.darNumeroElementos(); k++) {
							tag += " - " + tags.darElemento(k);
						}
						System.out.println(tag+"\n");
					}
					else
					{
						System.out.println(tag);
					}
				}
			}
		}
	}
	
	public static void imprimirRatings(ILista<VORating> lisRat, Long id)
	{
		if(lisRat == null)
		{
			System.out.println("No se encontr� una pelicula con ese ID");
		}
		else
		{
			int max = lisRat.darNumeroElementos();
			System.out.println("La pelicula con el ID "+ id +" posee " +max+ " ratings:"+"\n");
			for (int i = 0; i < max; i++) {
				VORating voR = lisRat.darElemento(i);
				System.out.println("Rating: " + voR.getRating() + " - Usuario: " + voR.getIdUsuario()+ " - Fecha: " + voR.getTimeStamp());
			}
		}
	}
	
}
