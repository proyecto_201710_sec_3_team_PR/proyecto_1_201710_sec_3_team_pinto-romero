package model.vo;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;
	private double diferencia;
	private double ratingAsinadoRecomendacion;

	
	public double getRatingAsinadoRecomendacion() {
		return ratingAsinadoRecomendacion;
	}

	public void setRatingAsinadoRecomendacion(double ratingAsinadoRecomendacion) {
		this.ratingAsinadoRecomendacion = ratingAsinadoRecomendacion;
	}
	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;
	
	public double getDiferencia() {
		return diferencia;
	}
	
	public void setDiferencia(double diferencia) {
		this.diferencia = diferencia;
	}
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	
	public long getIdUsuario() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	@Override
	public int compareTo(VOPelicula vo) 
	{
		//De menor a mayor a --> z
		return getTitulo().compareTo(vo.getTitulo());
	}
}
