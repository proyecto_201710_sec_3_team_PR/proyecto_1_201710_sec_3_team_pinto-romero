package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.Queue;
import model.data_structures.QuickSortList;
import model.data_structures.Stack;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{

	private ListaEncadenada<VOPelicula> misPeliculas;
	private ILista<VORating> misRatings;
	private ILista<VOUsuario> misUsuarios;
	private ILista<VOGeneroPelicula> misGeneros;
	private ILista<VOTag> misTags;
	private Stack<VOOperacion> misOperaciones;
	private Stack<String> misRecomendaciones; 
	private VORating rati;

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {

		BufferedReader br = null;
		misPeliculas = new ListaEncadenada<>();
		misGeneros = new ListaEncadenada<VOGeneroPelicula>();
		misRecomendaciones = new Stack<>();

		try
		{
			br = new BufferedReader(new FileReader(rutaPeliculas));
			String line = br.readLine();
			line = br.readLine();
			while(line != null)
			{
				ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
				String nombreP = "";
				int anioP = 0;
				String ge = line.substring(line.lastIndexOf(",")+1);
				String anNom = line.substring(0, line.lastIndexOf(","));

				if(ge.contains("\""))
				{
					ge = ge.replaceAll("\"", "");
				}

				String generos[] = ge.split("\\|");

				for(int i = 0; i < generos.length; i++ )
				{
					listaGeneros.agregarElementoFinal(generos[i]);
				}

				String anio = "";
				if (anNom.contains("("))
				{
					anio = anNom.substring(anNom.lastIndexOf("(")+1, anNom.lastIndexOf(")"));
					anNom = anNom.substring(0, anNom.lastIndexOf("("));
				}

				if(anNom.contains("\""))
				{
					nombreP = anNom.replaceAll("\"", "");
				}
				else 
				{
					nombreP = anNom;
				}

				if(!anio.isEmpty())
				{
					anioP = Integer.parseInt(anio.substring(0, 4));
				}
				agregarPelicula(nombreP, anioP, generos);
				line = br.readLine();
			}	
			br.close();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}


	@Override
	public boolean cargarRatingsSR(String rutaRatings) {
		BufferedReader br = null;
		misRatings = new ListaEncadenada<>();
		misUsuarios = new ListaEncadenada<>();

		try
		{
			br = new BufferedReader(new FileReader(rutaRatings));
			String line = br.readLine();
			line = br.readLine(); 
			while(line != null)
			{
				rati = new VORating();
				String[] datosRating = line.split(",");
				int idUsuario = Integer.parseInt(datosRating[0]);
				int idPelicula = Integer.parseInt(datosRating[1]);
				Double rating = Double.parseDouble(datosRating[2]);
				Long ls = Long.parseLong(datosRating[3]);
				Timestamp ts = new Timestamp(ls);
				rati.setTimeStamp(ts);

				agregarRating(idUsuario,idPelicula, rating);
				line = br.readLine();
			}
			br.close();
			return true;
		}
		catch (Exception E)
		{
			return false;
		}
	}


	@Override
	public boolean cargarTagsSR(String rutaTags) {
		BufferedReader br = null;
		misTags = new ListaEncadenada<>();
		try
		{
			br = new BufferedReader(new FileReader(rutaTags));
			String line = br.readLine();
			line = br.readLine(); 
			VOTag tag;
			while(line != null)
			{
				tag = new VOTag();
				String[] datosTags = line.split(",");
				String tagD = "";

				if(line.contains("\""))
				{
					tagD = line.substring(line.indexOf("\"")+1, line.lastIndexOf("\""));
				}
				else
				{
					tagD = datosTags[2];
				}

				int idUsuario = Integer.parseInt(datosTags[0]);
				int idPelicula = Integer.parseInt(datosTags[1]);
				Long ls = Long.parseLong(line.substring(line.lastIndexOf(",")+1));
				tag.setTag(tagD);
				tag.setMovieID(idPelicula);
				tag.setUserId(idUsuario);
				misTags.agregarElementoFinal(tag);
				agregarTagAPelicula(idPelicula, tag);
				line = br.readLine();
				Timestamp ts = new Timestamp(ls);
				tag.setTimestamp(ts);
			}
			br.close();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

	public boolean cargarRecomendaciones(String rutaRecomendacion)
	{
		BufferedReader br = null;
		try
		{
			br = new BufferedReader(new FileReader(rutaRecomendacion));
			String line = br.readLine();
			line = br.readLine();
			while(line != null)
			{
				misRecomendaciones.push(line);
				line = br.readLine();
			}
			br.close();
			return true;
		}
		catch (Exception e) {
			return false;
		}
	}

	public void cargarGeneros()
	{
		ListaEncadenada<VOPelicula> peliculasGenero;
		for(int i = 0; i < sizeMoviesSR(); i++)
		{
			VOPelicula peli = misPeliculas.darElemento(i);
			ILista<String> gene = peli.getGenerosAsociados();
			for (int j = 0; j < gene.darNumeroElementos(); j++)
			{
				String gen = gene.darElemento(j);
				boolean agrego = false;
				for (int k = 0; k < misGeneros.darNumeroElementos() && !agrego; k++) 
				{
					VOGeneroPelicula genePeli = misGeneros.darElemento(k);
					if(genePeli.getGenero().equals(gen))
					{
						genePeli.getPeliculas().agregarElementoFinal(peli);
						agrego = true;
					}
				}

				if(agrego == false)
				{
					VOGeneroPelicula genero = new VOGeneroPelicula();
					genero.setGenero(gen);
					peliculasGenero = new ListaEncadenada<>();
					peliculasGenero.agregarElementoAlInicio(peli);
					genero.setPeliculas(peliculasGenero);
					misGeneros.agregarElementoFinal(genero);
				}
			}	
		}
	}

	@Override
	public int sizeMoviesSR() {
		return misPeliculas.darNumeroElementos();
	}


	@Override
	public int sizeUsersSR() {
		return misUsuarios.darNumeroElementos();
	}


	@Override
	public int sizeTagsSR() {
		return misTags.darNumeroElementos();
	}

	public int sizeRecomendaciones()
	{
		return misRecomendaciones.size();
	}

	public int sizeGeneros()
	{
		return misGeneros.darNumeroElementos();
	}



	//Gabriel

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) 
	{
		if(misGeneros.darNumeroElementos() == 0) cargarGeneros();
		ILista<VOGeneroPelicula> misPelisPopulares = new ListaEncadenada<>();
		ILista<VOPelicula> pelisG;
		for (int i = 0; i < misGeneros.darNumeroElementos(); i++) {
			VOGeneroPelicula voG = misGeneros.darElemento(i);
			pelisG = voG.getPeliculas();
			QuickSortList.sort((ListaEncadenada<VOPelicula>) pelisG, QuickSortList.NRATING);
			VOGeneroPelicula voNG = new VOGeneroPelicula();
			ILista<VOPelicula> pelisNG = new ListaEncadenada<>();
			voNG.setGenero(voG.getGenero());
			for (int j = 0; j < n && j < pelisG.darNumeroElementos(); j++) {
				VOPelicula voP =  pelisG.darElemento(j);
				pelisNG.agregarElementoFinal(voP);
			}
			voNG.setPeliculas(pelisNG);
			misPelisPopulares.agregarElementoFinal(voNG);
		}
		return misPelisPopulares;
	}


	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {

		ILista<VOPelicula> ordenadas = new ListaEncadenada<>();
		ILista<VOPelicula> anios, ratings;
		VOPelicula voP;
		QuickSortList.sort((ListaEncadenada<VOPelicula>) misPeliculas, QuickSortList.FECHA);
		int anio = misPeliculas.darElemento(0).getAgnoPublicacion();
		boolean mismoAnio = true;
		boolean terminado = true;
		int agregar =0;
		int agregado = 0;
		int posicionComparar = 0;
		while(terminado)
		{
			anios = new ListaEncadenada<>();
			//Recorre las peliculas buscando las que poseen el mismo a�o y las agrega a una lista

			int max = 0;

			for (int i = posicionComparar; i < sizeMoviesSR() && mismoAnio; i++) 
			{
				voP = misPeliculas.darElemento(i);
				int anioP = voP.getAgnoPublicacion();
				max=i;
				if(anioP == anio)
				{
					anios.agregarElementoFinal(voP);
					agregado++;
				}
				else
				{
					posicionComparar = i;
					anio = anioP;
					mismoAnio = false;	
				}
			}	

			if(anios.darNumeroElementos() != 0 )
			{
				//Ordena las peliculas de un mismo a�o por rating
				QuickSortList.sort((ListaEncadenada<VOPelicula>) anios, QuickSortList.PQRATING);
				ratings = new ListaEncadenada<>();
				VOPelicula voPO = anios.darElemento(0); 
				ratings.agregarElementoFinal(voPO);				
				for (int j = 0; j < anios.darNumeroElementos(); j++) 
				{
					VOPelicula voPOC = anios.darElemento(j);

					if(voPO.getPromedioRatings() != voPOC.getPromedioRatings() || j == (anios.darNumeroElementos()-1))
					{
						if(ratings.darNumeroElementos() != 1)
						{
							//Ordena las de un mismo rating por nombre
							QuickSortList.sort((ListaEncadenada<VOPelicula>) ratings, QuickSortList.NOMBRE);
							VOPelicula peliOrdenada;
							for (int k = 0; k < ratings.darNumeroElementos(); k++) {
								peliOrdenada = ratings.darElemento(k);
								ordenadas.agregarElementoFinal(peliOrdenada);
								agregar++;
							}
						}
						else
						{
							ordenadas.agregarElementoFinal(ratings.darElemento(0));
							agregar++;
						}


						ratings = new ListaEncadenada<>();
						ratings.agregarElementoFinal(voPOC);	
						voPO = voPOC;
					}
					else
					{
						ratings.agregarElementoFinal(voPOC);
					}
				}

			}

			if(max == (sizeMoviesSR()-1))
			{
				terminado = false;
			}

			mismoAnio = true;

		}
		return ordenadas;
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		ListaEncadenada<VOGeneroPelicula> recomendadas = new ListaEncadenada<>();

		ListaEncadenada<VOPelicula> peliGenero;
		ILista<VOPelicula> pelisG;
		VOGeneroPelicula genePeli;
		VOGeneroPelicula voG;
		if(sizeGeneros() == 0)cargarGeneros();

		for (int i = 0; i < sizeGeneros(); i++) {
			peliGenero = new ListaEncadenada<>();
			voG = new VOGeneroPelicula();
			genePeli = misGeneros.darElemento(i);
			pelisG = genePeli.getPeliculas();
			QuickSortList.sort((ListaEncadenada<VOPelicula>) pelisG, QuickSortList.PQRATING);
			voG.setGenero(genePeli.getGenero());
			VOPelicula voP = pelisG.darElemento(0);
			peliGenero.agregarElementoFinal(voP);
			for (int j = 1; j < pelisG.darNumeroElementos(); j++) 
			{
				VOPelicula vo = pelisG.darElemento(j);
				if(vo.getPromedioRatings() == voP.getPromedioRatings())
				{
					peliGenero.agregarElementoFinal(vo);
				}
				else
				{
					j = pelisG.darNumeroElementos();
				}
			}
			voG.setPeliculas(peliGenero);
			recomendadas.agregarElementoFinal(voG);
		}
		return recomendadas;
	}


	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		if(misGeneros.darNumeroElementos() == 0) cargarGeneros();
		for (int i = 0; i < misGeneros.darNumeroElementos(); i++) {
			VOGeneroPelicula voG = misGeneros.darElemento(i);
			ILista<VOPelicula> voGP = voG.getPeliculas();
			QuickSortList.sort((ListaEncadenada<VOPelicula>) voGP, QuickSortList.FECHA);
		}
		return misGeneros;
	}


	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {

		if(sizeRecomendaciones() == 0)cargarRecomendaciones(rutaRecomendacion);
		ILista<VOPeliculaPelicula> voPPL = new ListaEncadenada<>();
		String recomendacion, gene;
		Double rating;
		VOPelicula voP;
		VOPeliculaPelicula voPP;
		Long idPelicula;
		Stack<String> recomendaciones = new Stack<>();
		int max = sizeRecomendaciones();

		for (int i = 0; i < n && i < max; i++) 
		{
			recomendacion = misRecomendaciones.pop();
			idPelicula = Long.parseLong(recomendacion.substring(0, recomendacion.indexOf(",")));
			rating = Double.parseDouble(recomendacion.substring(recomendacion.indexOf(",")+1));
			voP = buscarPeliculaId(idPelicula, misPeliculas);
			recomendaciones.push(recomendacion);
			if (voP != null) 
			{
				voP.setRatingAsinadoRecomendacion(rating);
				voPP = new VOPeliculaPelicula();
				voPP.setPelicula(voP);
				ILista<VOPelicula> pelisVOPP = new ListaEncadenada<>();
				gene = voP.getGenerosAsociados().darElemento(0);

				if (sizeGeneros() == 0) cargarGeneros();

				for (int j = 0; j < sizeGeneros(); j++) 
				{
					VOGeneroPelicula voG = misGeneros.darElemento(j);
					if (voG.getGenero().equalsIgnoreCase(gene)) 
					{
						ILista<VOPelicula> listGP = voG.getPeliculas();
						Double ratiP = 0.0;
						Double diferencia = 0.0;

						for (int k = 0; k < listGP.darNumeroElementos(); k++) 
						{
							VOPelicula peli = listGP.darElemento(k);

							ratiP = peli.getPromedioRatings();

							diferencia = (rating-ratiP);
							diferencia = (diferencia < 0 ? -diferencia : diferencia);

							if ( diferencia <= 0.5) 
							{
								peli.setDiferencia(diferencia);
								pelisVOPP.agregarElementoFinal(peli);
							}
						}
						QuickSortList.sort((ListaEncadenada<VOPelicula>) pelisVOPP, QuickSortList.DIFERAT);
						if(QuickSortList.estaOrdenada((ListaEncadenada<VOPelicula>) pelisVOPP, QuickSortList.DIFERAT))voPP.setPeliculasRelacionadas(pelisVOPP);
					}
				}
				voPPL.agregarElementoFinal(voPP);
			}
		}
		while(!recomendaciones.isEmpty())misRecomendaciones.push(recomendaciones.pop());
		return voPPL;
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		VOPelicula peli = buscarPeliculaId(idPelicula, misPeliculas);
		ILista<VORating> ratisPel = null;

		if (peli != null) {
			ratisPel = new ListaEncadenada<>();
			for (int i = 0; i < misRatings.darNumeroElementos(); i++) 
			{
				VORating voR = misRatings.darElemento(i);
				if(voR.getIdPelicula() == idPelicula)
				{
					ratisPel.agregarElementoFinal(voR);
				}
			}
			QuickSortList.sort((ListaEncadenada<VORating>) ratisPel, QuickSortList.QTRATING);
		}

		return ratisPel;
	}


	//Juan Pablo


	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		
		
		
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) 
	{
		ILista<VOGeneroPelicula> lista = new ListaEncadenada<>();
		for (int i = 0; i < misGeneros.darNumeroElementos(); i++) 
		{
			VOGeneroPelicula vo = new VOGeneroPelicula();
			ILista<VOPelicula> listaPelis = new ListaEncadenada<>();
			ILista<VOPelicula> pelisGeneroAct = misGeneros.darElemento(i).getPeliculas();
			VOPelicula peli = null;
			QuickSortList.sort( (ListaEncadenada<VOPelicula>) pelisGeneroAct, QuickSortList.NRATING);
			for(int j = 0; j < n; j++)
			{
				peli = pelisGeneroAct.darElemento(j);
				listaPelis.agregarElementoFinal(peli);
			}
			vo.setGenero(misGeneros.darElemento(i).getGenero());
			vo.setPeliculas(listaPelis);
			lista.agregarElementoFinal(vo);
		}


		return lista;
	}


	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() 

	{
	
		QuickSortList.sort( (ListaEncadenada<VOPelicula>) misPeliculas, QuickSortList.FECHA);
		ILista<VOUsuarioGenero> ret = new ListaEncadenada<VOUsuarioGenero>();
		for (int i = 0; i < misUsuarios.darNumeroElementos(); i++) 
		{
			
			VOUsuarioGenero usuariogenero = new VOUsuarioGenero();
			ILista<VOGeneroTag> listaInterna = new ListaEncadenada<VOGeneroTag>();
			VOUsuario usuarioAct = misUsuarios.darElemento(i);
			VOGeneroPelicula generoAct = null;
			VOGeneroTag generotag = new VOGeneroTag();

			for (int j = 0; j < misGeneros.darNumeroElementos(); j++) 
			{
				ListaEncadenada<String> tags = new ListaEncadenada<String>();
				generoAct = misGeneros.darElemento(j);
				for (int k = 0; k < misTags.darNumeroElementos(); k++) 
				{
					VOTag tagActual = misTags.darElemento(k);
					String tagValido = null;

					for (int l = 0; l < misPeliculas.darNumeroElementos(); l++) 
					{
						VOPelicula peli = misPeliculas.darElemento(l);
						if (peli.getIdPelicula() == tagActual.getMovieID())
						{

							for (int m = 0; m < peli.getGenerosAsociados().darNumeroElementos(); m++) 
							{
								String gen = peli.getGenerosAsociados().darElemento(m);
								if (gen.equals(generoAct.getGenero()))
								{
									tagValido = tagActual.getTag();

								}
							}

						}
						if(tagValido != null)
						{
							tags.agregarElementoAlInicio(tagValido);
						}
					}

					generotag.setGenero(generoAct.getGenero());
					generotag.setTags(tags);
					listaInterna.agregarElementoFinal(generotag);
				}
			}



			usuariogenero.setIdUsuario(usuarioAct.getIdUsuario());                                                                                                                                         
			usuariogenero.setListaGeneroTags(listaInterna);
			ret.agregarElementoFinal(usuariogenero);		

		}

		return ret;
	}


	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) 
	{
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) 
	{
		ILista<VOTag> lista = new ListaEncadenada<VOTag>();
		
		for (int i = 0; i < misTags.darNumeroElementos(); i++) 
		{
			if(misTags.darElemento(i).getMovieID() == idPelicula)
			{
				lista.agregarElementoFinal(misTags.darElemento(i));
			}
		}
		QuickSortList.sort((ListaEncadenada<VOTag>) lista, QuickSortList.PQRATING);
		
		return lista;
	}


	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		VOOperacion oper = new VOOperacion();
		oper.setOperacion(nomOperacion);
		oper.setTimestampInicio(tinicio);
		oper.setTimestampFin(tfin);
		misOperaciones.push(oper);
	}


	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() {
		return misOperaciones.darListaStack();
	}


	@Override
	public void limpiarHistorialOperacionesSR() 
	{
		misOperaciones = new Stack<>();
	}


	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) {

		ILista<VOOperacion> listaOper = darHistoralOperacionesSR();
		ILista<VOOperacion> ultiOper = new ListaEncadenada<>();
		for (int i = 0; i < n; i++) {
			VOOperacion vOper = listaOper.darElemento(i);
			ultiOper.agregarElementoFinal(vOper);
		}

		return ultiOper;
	}


	@Override
	public void borrarUltimasOperaciones(int n) {
		for (int i = 0; i < n; i++) {
			misOperaciones.pop();
		}
		misOperaciones.darListaStack().modificarIndex();
	}


	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) {

		VOPelicula peli = new VOPelicula();

		ListaEncadenada<String> listaGeneros = new ListaEncadenada<String>();
		for(int i = 0; i < generos.length; i++ )
		{
			listaGeneros.agregarElementoFinal(generos[i]);
		}

		int coma = titulo.indexOf(",");
		Long id = Long.parseLong(titulo.substring(0, coma));
		peli.setTitulo(titulo.substring(coma+1));
		peli.setAgnoPublicacion(agno);
		peli.setGenerosAsociados(listaGeneros);
		peli.setIdPelicula(id);
		misPeliculas.agregarElementoFinal(peli);

		return id;
	}


	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		rati.setIdPelicula(idPelicula);
		rati.setIdUsuario(idUsuario);
		rati.setRating(rating);
		VOPelicula peli = buscarPeliculaId((long)idPelicula, misPeliculas);
		int nR = peli.getNumeroRatings();
		peli.setNumeroRatings(nR+1);
		double rats = ((peli.getPromedioRatings()*nR)+rating)/(nR+1);
		peli.setPromedioRatings(rats);
		misRatings.agregarElementoFinal(rati);
	}


	public VOPelicula buscarPeliculaId(Long idPelicula, ListaEncadenada<VOPelicula> pelis)
	{
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		while(iter.hasNext())
		{
			VOPelicula peli = iter.next();
			if(peli.getIdPelicula() == idPelicula)
			{
				return peli;
			}
		}
		return null;
	}


	public void agregarTagAPelicula(int idPelicula, VOTag tag)
	{
		VOPelicula voP = buscarPeliculaId((long) idPelicula, misPeliculas);
		if(voP != null)
		{
			if(voP.getNumeroTags() == 0)
			{
				ILista<String>tagsVOP = new ListaEncadenada<>();
				tagsVOP.agregarElementoFinal(tag.getTag());
				voP.setTagsAsociados(tagsVOP);
				voP.setNumeroTags(1);
			}
			else
			{
				voP.getTagsAsociados().agregarElementoFinal(tag.getTag());
				voP.setNumeroTags(voP.getNumeroTags()+1);
			}
		}
	}
}
